import imutils
import cv2
import numpy as np

#img = cv2.imread("/home/jackie13/Pictures/minithed.png")
img = cv2.imread('/home/jackie13/Pictures/tresholded.png')
gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
ret,thresh = cv2.threshold(gray_image,127,255,0)

im2, contours, hierarchy = cv2.findContours(thresh,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)
#print(contours)

pre_cont = np.vstack(contours).squeeze()
#print(pre_cont)

cnvt_cont = tuple(tuple(x) for x in pre_cont)
#print(cnvt_cont)

for c in contours:
	# calculate moments for each contour
	M = cv2.moments(c)

# calculate x,y coordinate of center
cX = int(M["m10"] / M["m00"])
cY = int(M["m01"] / M["m00"])
cv2.circle(img, (cX, cY), 2, (255, 15, 55), -1)
cv2.putText(img, "centroid", (cX - 25, cY - 25),cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 15, 55), 2)


#create list of maximum "distance" between center,6 egde of hexagon
sizearray = len(cnvt_cont)
pre_d  = [[0,0]] * sizearray

ori_real_d = [0] * sizearray
sort_rd    = [0] * sizearray

for i in range(sizearray):
	pre_d[i][0]   = ((cX - cnvt_cont[i][0])**2)

	pre_d[i][1]   = ((cY - cnvt_cont[i][1])**2)

        ori_real_d[i] = np.sqrt(pre_d[i][0] + pre_d[i][1])
	sort_rd[i]    = np.sqrt(pre_d[i][0] + pre_d[i][1])

sort_rd.sort(reverse=True)

#function convert coordinate x,y to data for calculate dimention
def pxtobi(x,y):
	pixel= img[x, y]
	print('L = ',pixel[0])
	return pixel



#need index to get the x,y from contour list
hexapoints = [[0,0]]*len(range(0,6))


for i in range(sizearray):
	for j in range(0,5):
		if sort_rd[i] == ori_real_d[j]:
			print(i)
			print(cnvt_cont[i])
			x,y = cnvt_cont[i]
			pxtobi(x,y) 
"""
			cv2.circle(img, cnvt_cont[i] , 2, (255, 15, 55), -1)
			cv2.putText(img, "x,y,l = %s %s %s", (cX - 25, cY - 25)
			,cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 2)
"""

#print hexapoints
#display the image
cv2.imshow("Image", img)
cv2.waitKey(0)
