import freenect
import cv2
import numpy as np
#draw
cirthick    = 4

color2      = (125,27,255)			#color for dot
color4      = (0,150,50)			#color for text

#font       =  cv2.FONT_HERSHEY_SIMPLEX
font        =  cv2.FONT_HERSHEY_SCRIPT_COMPLEX

####### START 1/2 ####################3

#color
fx_rgb = 5.2921508098293293e+02
fy_rgb = 5.2556393630057437e+02
cx_rgb = 3.2894272028759258e+02
cy_rgb = 2.6748068171871557e+02
k1_rgb = 2.6451622333009589e-01
k2_rgb = -8.3990749424620825e-01
p1_rgb = -1.9922302173693159e-03
p2_rgb = 1.4371995932897616e-03
k3_rgb = 9.1192465078713847e-01
#depth
fx_d = 5.9421434211923247e+02
fy_d = 5.9104053696870778e+02
cx_d = 3.3930780975300314e+02
cy_d = 2.4273913761751615e+02
k1_d = -2.6386489753128833e-01
k2_d = 9.9966832163729757e-01
p1_d = -7.6275862143610667e-04
p2_d = 5.0350940090814270e-03
k3_d = -1.3053628089976321e+00

#Relative transform between the sensors (in meters)
RR = np.array([ 
  [ 9.9984628826577793e-01, 1.2635359098409581e-03, -1.7487233004436643e-02,], 
  [-1.4779096108364480e-03, 9.9992385683542895e-01, -1.2251380107679535e-02,],
  [ 1.7470421412464927e-02, 1.2275341476520762e-02,  9.9977202419716948e-01 ]  
    ])

TT = np.array([1.9985242312092553e-02, -7.4423738761617583e-04,-1.0916736334336222e-02])

################# END 1/2

def drawCircle(xymax):
    #cv2.circle(depth, xymax, cirthick, color2, -1)
    cv2.circle(frame, xymax, cirthick, color2, -1)

def drawCircle_D(xymax):
    cv2.circle(depth, xymax, cirthick, color2, -1)
   
#function to get RGB image from kinect
def get_video():
    array,_ = freenect.sync_get_video()
    array = cv2.cvtColor(array,cv2.COLOR_RGB2BGR)
    return array
 
#function to get depth image from kinect
def get_depth():
    array,_ = freenect.sync_get_depth()
    array = array.astype(np.uint8)
    return array

######################################## START 2/2  ########

def D2RGB_align(x,y):
        #array depth is  depth[y][x] 
        P3D_x = ( x - cx_d )*(depth[y][x]) / fx_d
        P3D_y = ( y - cy_d )*(depth[y][x]) / fy_d
        P3D_z = depth[y][x]
        
        #new_P3D = Rotation*P3D + T
        P3D = np.array( [P3D_x , P3D_y , P3D_z] )
        new_P3D = np.dot(RR , P3D) - TT
        
        rgb_x = (new_P3D[0] * fx_rgb / new_P3D[2]) + cx_rgb 

        rgb_y = (new_P3D[1] * fy_rgb / new_P3D[2]) + cy_rgb

        rgb_xy = int(rgb_x),int(rgb_y) 
        return list(rgb_xy)

is_done  = 0 #variable for

if __name__ == "__main__":
    while 1:
        #get a frame from RGB camera
        frame = get_video()
        #get a frame from depth sensor
        depth = get_depth()

        if is_done == 0 :
            #d2rgb = []
            d2rgb = []

            for x in range(315,325):
                for y in range(30,40):
                    d2rgb = d2rgb + D2RGB_align(x,y)

            list3D_rgb = np.array(d2rgb).reshape(10,10,2)
            
            is_done = 1


        depth_xy = 320,35 #xy at the top mid of box
        color_xy = 304,75 #this too
        for y in range(10):
            for x in range(10):
                x_tuple = list3D_rgb[y][x][0] #[y][x][0] mean run y0-end x0-end   [0] mean X
                y_tuple = list3D_rgb[y][x][1] #                                   [1] mean Y   
                #if ( x_tuple - color_xy[0] >= -1  )|( x_tuple - color_xy[0] >= 1  ) :        conditionif i need more +- 1
                #    if ( y_tuple - color_xy[1] >= -1  )|( y_tuple - color_xy[0] >= 1  ) :
                if (tuple(list3D_rgb[y][x]) == color_xy) : #array D2rgb compare RGB points
                    drawCircle(color_xy)
                    drawCircle_D((x+315,y+40))
                    drawCircle_D(color_xy) 


###### END 2/2 #############################3

        #display RGB image
        cv2.imshow('RGB image',frame)
        #display depth image
        cv2.imshow('Depth image',depth)
 
        # quit program when 'esc' key is pressed
        k = cv2.waitKey(5) & 0xFF
        if k == 27:
            break
    cv2.destroyAllWindows()