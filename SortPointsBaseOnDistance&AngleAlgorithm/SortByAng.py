import numpy as np

#cherprang
cX = 421
cY = 263
#fake contour for test
cnvt_cont = ((300,200),(250,450),(500,400),(100,50),(200,100),(1100,990))
sizearray = len(cnvt_cont)

#find all """coordinate"""  was created by 'points of contour' - 'cherprang'
#find all """distance"""    of that coordinate
pre_points = [[0,0]]*sizearray
points     = [[0,0]]*sizearray

pre_d	   = [[0,0]]*sizearray
ori_d      = [[0,0]]*sizearray
sort_d     = [[0,0]]*sizearray

for i in range(sizearray):
	#find points
	pre_points[i][0] = cnvt_cont[i][0] - cX
	pre_points[i][1] = cnvt_cont[i][1] - cY
	points[i]        = [pre_points[i][0],pre_points[i][1]] #original point
	#calculate distance
	pre_d[i][0]      = ((cnvt_cont[i][0] - cX)**2)
	pre_d[i][1]      = ((cnvt_cont[i][1] - cY)**2)
	ori_d[i]         = np.sqrt(pre_d[i][0] + pre_d[i][1]) #original distance
	sort_d[i]        = np.sqrt(pre_d[i][0] + pre_d[i][1]) #prepare sorted   distance
sort_d.sort(reverse=True)

#find coordinate x,y which keep most distance
#prepare array
sorted_points = [[0,0]]*sizearray #prepare for find angle
#loop for find x,y
for i in range(sizearray):
	for j in range(sizearray):
		if sort_d[i] == ori_d[j]:		#when index original = 5
			sorted_points[i] = cnvt_cont[j]	#contour index = 5 same
#			print('sorted point number %s is point %s : %s'%(i,j,sorted_points[i]))



def get_angle(p0,p1):
    	''' compute angle (in degrees) for p0p1p2 corner
    	Inputs:
        p0,p1,p2 - points in the form of [x,y]
    	'''
    	v0 = np.array(p0)
    	v1 = np.array(p1)

	angle1 = np.math.atan2(np.linalg.det([v0,v1]),np.dot(v0,v1))
	angle2 = np.degrees(angle1)
	angle3 = np.sqrt((angle2)**2) #dont care if -    just need +value to compare
	#print angle
	return angle3



#algorithm to find 6 ext points of hexagon
maximumline = [[0,0]]*10   					#10 cuase need to do this algorithm again and then it go 6    may be?
sizeline = len(maximumline)

maximumline[0] = sorted_points[0]
m = 0 								#index for max line

L1 = 0 								# 0-9 if want to find 10 longest distance
L2 = 0

adjust_ang = 15

for l in range(sizearray):  					#loop all point in contour
	AngBtwLine = get_angle(sorted_points[L1],sorted_points[L2])
	if AngBtwLine > adjust_ang : 				#if angle > 15 that is line i need
		print('+')
		print('old L1 : %s     old L2 : %s  '%(L1,L2))
		print('angle between L1,L2 is : .3%f deg'%(AngBtwLine))

		#drawline

		l = l+1						#go to next Coordinate of contour

		maximumline[m] = points[L2]			# put contour to new array
		m = m+1

		L1 = L2						#then reference line is go to next
		L2 = L2+1					#and  referenced line go next too
		print('found new max line')
		print('new L1 = %s    new L2 = %s '%(L1,L2))
		print('+')
	else:
		L1 = L1						#ref line is the same but  refed line is go next
		L2 = L2+1
		print('-')
		print('not found new max line')
		print('-')

print(maximumline)