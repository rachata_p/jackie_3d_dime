def L_to_distance(L):
    Distance = (L*0.638)+113.57 #Dstep = 0.638    D1=113.57  see algor in A4 paper
    return round(Distance,3)
