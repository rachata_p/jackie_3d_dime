import numpy as np
import cv2

largest_area=0;
largest_contour_index=0

#im = cv2.imread("/home/jackie13/Pictures/captestkinect/6april/after_mask/masked01.png")
im = cv2.imread("/home/jackie13/Pictures/captestkinect/13april/masked_box.png")

imgray = cv2.cvtColor(im, cv2.COLOR_BGR2GRAY)
ret, thresh = cv2.threshold(imgray, 127, 255, 0)
im2, contours, hierarchy = cv2.findContours(thresh, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

c = max(contours, key = cv2.contourArea)

cv2.drawContours(im, c, -1, 255, 3)
cv2.imshow('largest contour ',im)
cv2.waitKey()
cv2.destroyAllWindows()